package org.cuatrovientos.ed;

public class Principal {

	public static void main(String[] args) {

		Product p0 = new Product("manzanas", 5, 12.1f);
		Product p1 = new Product("alubias", 50, 100.1f);
		Product p2 = new Product("pan", 2, 2.1f);
		
		Invoice factura = new Invoice("Ander");
		factura.add(p0);
		factura.add(p1);
		factura.add(p2);
		
		float precioTotal = factura.total();
		System.out.println("El precio total es " + precioTotal);
		
		factura.remove(1);
		
		System.out.println("El nuevo precio es: " + factura.total());
		
	}

}
