package org.cuatrovientos.ed;

public class Invoice {

	private String customer;
	
	public Product[] listaProductos;

	public Invoice(String customer) {
		super();
		this.customer = customer;
		
		// Construir mi lista de productos, para evitar NULL POINTER EXCEPTION
		this.listaProductos = new Product[10];
	}
	
	public void add(Product p) {
		// TODO A�ADIR
		
		// Est� mi lista llena?
		if (this.listaProductos[9] == null) {
			// no est� llena
			// Tengo que localizar la �ltima posici�n vac�a
			for (int i = 0; i < listaProductos.length; i++) {
				if (this.listaProductos[i] == null) {
					this.listaProductos[i] = p;
					break;
				}
			}
		}
	}
	
	public void remove(int id) {
		// TODO ELIMINAR
		
		this.listaProductos[id] = null;
	}
	
	public float total() {
		float result = 0 ;
		for (Product product : listaProductos) {
			if (product != null) {				
				result += product.total();
			}
		}
		// TODO precio
		return result;
	}
	
}
