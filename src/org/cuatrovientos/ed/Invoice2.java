package org.cuatrovientos.ed;

import java.util.ArrayList;

public class Invoice2 {

	private String customer;
	
	public ArrayList<Product> listaProductos;
	
	public Invoice2(String customer) {
		super();
		this.customer = customer;
		listaProductos = new ArrayList<Product>();
	}
	
	public void add(Product p) {
		this.listaProductos.add(p);
	}
	
	public void remove(int id) {
		this.listaProductos.remove(id);
	}
	
	public float total() {
		float resultado = 0;
		for (Product product : listaProductos) {
			product.total();
			resultado += product.total();
		}
		return resultado;
	}
}
